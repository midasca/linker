<?php namespace App\Domain\Link;

use App\Infrastructure\Links\EloquentLinkRepository;
use App\Infrastructure\Links\LinkPlacer;

class LinkManager {

    protected $linkPlacer;

    protected $linkRepo;


    public function __construct(LinkPlacer $linkPlacer, EloquentLinkRepository $linkRepo)
    {
        $this->linkPlacer = $linkPlacer;

        $this->linkRepo = $linkRepo;
    }


    public function addLink($url, $anchor)
    {
        $url = $this->addHttp($url);

        if ( ! $pageUrl = $this->linkRepo->getPageUrlByLinkUrl($url) )
        {
            $pageUrl = $this->linkPlacer->addLink($url, $anchor);
        }

        return $pageUrl;
    }


    protected function addHttp($url): string
    {
        if ( ! stristr($url, "http://") )
        {
            $url = 'http://' . $url;
        }

        return $url;
    }

}