<?php namespace App\Infrastructure\Links;


use App\Infrastructure\Links\Exceptions\LinkPlacerException;
use App\Infrastructure\Pages\EloquentPageRepository;

class LinkPlacer {


    protected $linkRepo;

    protected $pageRepo;

    public function __construct(EloquentPageRepository $pageRepo, EloquentLinkRepository $linkRepo)
    {
        $this->pageRepo = $pageRepo;

        $this->linkRepo = $linkRepo;
    }


    public function addLink($url, $anchor)
    {
        $result = "";
        $times = 0;
        while (! $result)
        {
            $this->triedTooMany($times);
            $times ++;

            list($page, $result) = $this->addLinkToFile($url, $anchor, $result);
        }

        $this->linkRepo->addLink($url, $anchor, $page);

        return $page->url;
    }


    protected function getFullFilePath($page)
    {
        $fullFilePath = $page['path'] . '/' . $page['filename'];
        $fullFilePath = str_replace('//', '/', $fullFilePath);

        return $fullFilePath;
    }

//https://stackoverflow.com/questions/1252693/using-str-replace-so-that-it-only-acts-on-the-first-match
    protected function str_replace_first($search, $replace, $content)
    {
        $search = '/' . preg_quote($search, '/') . '/';

        return preg_replace($search, $replace, $content, 1);
    }

    /**
     * @param $times
     * @throws LinkPlacerException
     */
    protected function triedTooMany($times): void
    {
        if ( $times == 5 )
        {
            throw new LinkPlacerException('Couldn\'t add link after 5 tries');
        }
    }



    protected function addLinkToFile($url, $anchor, $result): array
    {
        $page = $this->pageRepo->getRandomPageWithoutLink();
        
        if ( is_file($page->path) )
        {

            $result = $this->str_replace_first($page->marker, $page->marker . "<a href=$url>$anchor</a>", file_get_contents($page->path));

            if ( $result )
            {
                $result = file_put_contents($page->path, $result);
            }
        }

        return array($page, $result);
    }
}