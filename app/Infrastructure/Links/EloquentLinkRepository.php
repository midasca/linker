<?php namespace App\Infrastructure\Links;


use App\Infrastructure\Links\Exceptions\EloquentLinkRepositoryException;

class EloquentLinkRepository {

    public function addLink($url, $anchor, $page)
    {
        $link = new Link;
        $link->url = $url;
        $link->anchor = $anchor;

        $link->page()->associate($page);

        if ( $link->validate() )
        {
            return $link->save();
        } else
        {
            throw new EloquentLinkRepositoryException("Couldnt pass validation, " . $link->errors(), 1);
        }
    }


    public function getPageUrlByLinkUrl($url)
    {
        $link = Link::where('url', $url)->first();
        if ( ! $link )
        {
            return false;
        }

        return $link->page()->first()->toArray()['url'];
    }


}