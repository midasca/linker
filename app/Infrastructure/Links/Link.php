<?php namespace App\Infrastructure\Links;


use App\Infrastructure\Pages\Page;
use Illuminate\Database\Eloquent\Model;
use Validator;

class Link extends Model {

    protected $errors;

    protected $table = 'links';

    protected $guarded = [];

    private $rules = [
        'url'    => 'required|url|max:255',
        'anchor' => 'required|min:1'
    ];


    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    public function validate()
    {
        $v = Validator::make($this->attributes, $this->rules);

        return $v->passes();
    }


    public function errors()
    {
        return $this->errors;
    }
}