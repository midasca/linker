<?php namespace App\Infrastructure\Pages;


class PageParser {

    protected $pageRepo;

    public function __construct(EloquentPageRepository $pageRepo)
    {
        $this->pageRepo = $pageRepo;
    }


    public function process($input)
    {
        $input = $this->addSlashToBasePath($input);

//        $files = glob($input['basePath'] . '*');
////        dd($files);
//        $i = 1;
//        foreach ($files as $file)
//        {
//            if ( is_file($file) )
//            {
//                rename($file, $input['basePath'] . $i . '.html');
//                $i ++;
//            }
//        }
//        dd($files);
//        'url' => 'http://www.tranny.one',
//        'path' => '/Users/midas/Laravel/tranny.one/tranny.one/index.html',
//        'marker' => '<h3 class="block-title" style="margin-bottom: 0;">To upload your video, you need to be <a href="#" class="openLoginForm">Logged In</a></h3>'
        $this->validateBaseUrl($input);
        $filenames = $this->explodeVar($input['filenames']);

        foreach ($filenames as $filename)
        {
            $this->pageRepo->save($input['baseUrl'] . $filename, $input['basePath'] . $filename, $input['marker']);
        }

        return count($filenames);
    }


    protected function explodeVar($text): array
    {
        $array = explode("\n", $text);
        $array = array_filter($array);
        $array = array_map('trim', $array);

        return $array;
    }

    /**
     * @param $input
     */
    protected function validateBaseUrl($input): void
    {
        if ( ! stristr($input['baseUrl'], 'http://') )
        {
            dd('Base URL must contain http://');
        }
    }

    /**
     * @param $input
     * @return mixed
     */
    protected function addSlashToBasePath($input)
    {
        if ( substr($input['basePath'], - 1) != '/' )
        {
            $input['basePath'] = $input['basePath'] . '/';
        }

        return $input;
    }

}