<?php namespace App\Infrastructure\Pages;


use App\Infrastructure\Links\Link;
use Illuminate\Database\Eloquent\Model;
use Validator;

class Page extends Model {

    protected $errors;

    protected $table = 'pages';

    protected $guarded = [];

    private $rules = [
        'url'    => 'required|url|max:255',
        'path' => 'required|min:1',
        'marker' => 'required|min:1'
    ];


    public function links()
    {
        return $this->hasMany(Link::class);
    }


    public function validate()
    {
        $v = Validator::make($this->attributes, $this->rules);

        return $v->passes();
    }


    public function errors()
    {
        return $this->errors;
    }
}