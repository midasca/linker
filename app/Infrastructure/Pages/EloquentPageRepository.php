<?php namespace App\Infrastructure\Pages;


use App\Infrastructure\Pages\Exceptions\EloquentPageRepositoryException;

class EloquentPageRepository {

    public function save($url, $path, $marker)
    {
        $page = new Page;
        $page->url = $url;
        $page->path = $path;
        $page->marker = $marker;

        if ( $page->validate() )
        {
            return $page->save();
        } else
        {
            throw new EloquentPageRepositoryException("Couldnt pass validation, " . $page->errors(), 1);
        }
    }


    public function getRandomPageWithoutLink()
    {
        return Page::doesntHave('links')->get()->random();
    }
}