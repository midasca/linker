<?php namespace App\Http\Controllers;

// TODO add robots.txt to tranny sites
// TODO add link removal functionality
// TODO add Tests

use App\Domain\Link\LinkManager;
use App\Infrastructure\Pages\PageParser;
use Illuminate\Http\Request;
use View;

class DomainController extends Controller {

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function addLink(LinkManager $linkManager)
    {
//        $this->request->merge(['url' => 'http://www.google.com', 'anchor' => 'our bitch']);
        $this->validate($this->request, ['url' => 'required|min:1', 'anchor' => 'required|min:1']);

        $url = $linkManager->addLink($this->request->input('url'), $this->request->input('anchor'));

        return response()->json([
            'url' => $url,
        ], 200);
    }


    public function addLinkStub()
    {
        return response()->json([
            'url' => 'http://transgalleries.com',
        ], 200);
    }

    public function parsePageCredentials(PageParser $pageParser)
    {
        $total = $pageParser->process($this->request->all());
        dd("Done, $total page(s) were saved");
    }


    public function makeParsePageCredentials()
    {
        return View::make('parsePageCredentials');
    }
}