<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
//        switch (true)
//        {
//            case  $exception instanceof HttpException:
//                $response['message'] = Response::$statusTexts[$e->getStatusCode()];
//                $response['status'] = $e->getStatusCode();
//                break;
//
//            case $exception instanceof EngineAllowedException:
//                $response['status'] = 501;
//                $response['code'] = 40;
//                break;
//        }

        $response = [
            'message' => (string) $exception->getMessage(),
            'status'  => 400,
        ];

        return response()->json(['error' => $response], $response['status']);
//        return parent::render($request, $exception);
    }
}
