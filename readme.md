API that allows to add a link to questionable nature site (PORN!) to evoke reciprocity from business owners for reporting it
To download site 'wget --mirror -p --html-extension --page-requisites --adjust-extension inexpensivetreecare.com'

#####Calls should request JSON (header 'Accept' => 'application/json')


>#####Guzzle example

>>  $client = new Client([
              'base_uri' => $credentials['apiUrl'],
              'headers'  => ['content-type' => 'application/json', 'Accept' => 'application/json']
          ]);

>> base_uri = "http://sitesfree.net/linker/api/"

&nbsp;
&nbsp;


>### Returns all urls that rank for provided keyword
> ####Route: /addLink

>>
>> @param $url string / URL of the target site
>>
>> @param $anchor string / Anchor
>>

>##### @return JSON

>> @var JSON['url'] string / URL of the page with link
>>
***
&nbsp;
&nbsp;