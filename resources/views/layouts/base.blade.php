<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    @section('head')
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
              integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
              crossorigin="anonymous">
        <link href="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css"
              rel="stylesheet"/>
        <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
        @yield('scripts')
        <style>
            @import url(//fonts.googleapis.com/css?family=Lato:700);

            body {
                margin: 0;
                font-family: 'Lato', sans-serif;

                text-align: center;
                color: #999;
            }

            .table-hover > tbody > tr:hover {
                background-color: #cce6ff;
            }

            .table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
                background-color: #e6f2ff;
            }

            .table th, td {
                text-align: center;
            }

            a, a:visited {
                text-decoration: none;
            }

            .smaller {
                font-size: 80%;
            }

            .table-nonfluid {
                width: auto !important;
            }

            .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
                background-color: lightblue;
            }

            ul.nav a:hover {
                color: dodgerblue !important;
                background-color: lightblue;
            }

            #footer {
                position: fixed;
                bottom: 0;
                width: 100%;
            }

        </style>

        <script>

            $(document).ready(function () {

                $('#table').dataTable({
                    "order": [[3, "asc"]],
                    "pageLength": 50,
                    "columnDefs": [
                        {type: 'natural', targets: '_all'}
                    ],
                });


            });

        </script>


    @show
</head>
<body>

{{--<nav class="navbar navbar-default navbar-fixed-top">--}}
    {{--<div class="container-fluid">--}}

        {{--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">--}}
            {{--<ul class="nav navbar-nav">--}}
                {{--<li class={{ Request::is('search') ? 'active' : '' }}><a href="{{ url('marketplace/search') }}">Add--}}
                        {{--links</a></li>--}}
                {{--<li class={{ Request::is('sites/all') ? 'active' : '' }}><a href="{{ url('sites/all') }}">Money--}}
                        {{--sites</a></li>--}}
                {{--<li class={{ Request::is('/') ? 'active' : '' }}><a href="{{ url('/') }}">Process donors</a></li>--}}
                {{--<li class={{ Request::is('donors') ? 'active' : '' }}><a href="{{ url('donors') }}">Donors</a></li>--}}
                {{--<li class={{ Request::is('credentials') ? 'active' : '' }}><a href="{{ url('credentials') }}">Credentials</a>--}}
                {{--</li>--}}
                {{--<li class={{ Request::is('schedules') ? 'active' : '' }}><a href="{{ url('schedules') }}">Scheduled--}}
                        {{--links</a></li>--}}
            {{--</ul>--}}
            {{--<ul class="nav navbar-nav navbar-right">--}}
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"--}}
                       {{--aria-expanded="false">Tools<span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu" role="menu">--}}
                        {{--<li><a href="{{ url('tools/chooseSeoData') }}">Update SEO data</a></li>--}}
                        {{--<li><a href="{{ url('tools/showAhrefs') }}">Update AHREFS data</a></li>--}}
                        {{--<li><a href="{{ url('donors/tools') }}">Donors Tools</a></li>--}}
                        {{--<li role="separator" class="divider"></li>--}}
                        {{--<li><a href="{{ url('tools/moz') }}">Check any URL with Moz/DomDetailer</a></li>--}}
                        {{--<li><a href="{{ url('tools/kwCalculator') }}">Calculate KW density</a></li>--}}
                        {{--<li><a href="{{ url('tools/penalizedChecker') }}">Penalty Checker</a></li>--}}
                        {{--<li><a href="{{ url('tools/ips') }}">IPs Detector</a></li>--}}
                        {{--<li><a href="{{ url('tools/urlLookup') }}">Url Lookup</a></li>--}}
                        {{--<li><a href="{{ url('tools/domainsExtractor') }}">Domains Extractor</a></li>--}}
                        {{--<li role="separator" class="divider"></li>--}}
                        {{--<li><a href="{{ url('tools/showPenalized') }}">Show penalized domains</a></li>--}}
                        {{--<li><a href="{{ url('tools/domainsFirstPages') }}">Check db for non-root URLs in site:domain--}}
                                {{--request</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</nav>--}}

<br><br>
<p align="center">
@if(Session::has('error'))
    <div class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</div>
@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/alertSlideUp.js') }}"></script>
@stop
@endif

@yield('body')


</body>
</html>
