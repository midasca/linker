@extends('layouts.base')
@section('title', 'Submit Pages for processing')
@section('body')
    <br><br>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <p align="center">
        {!! Form::open(array('url' => 'parsePage', 'target' => '_blank', 'autocomplete' => "off")) !!}

        <font color="purple">
            {!! Form::label('Base URL') !!} (with http:// !!):
        </font>
        {!! Form::text('baseUrl', null, ['size' => 50, 'maxlength' => 250, 'placeholder' => 'with subfolders, if applicable']) !!}
        <br><br>
        <font color="purple">
            {!! Form::label('Base Path') !!} :
        </font>
        {!! Form::text('basePath', null, ['size' => 50, 'maxlength' => 250, 'placeholder' => 'with subfolders, if applicable']) !!}
        <br><br>
        <font color="purple">
            {!! Form::label('File Names') !!}
        </font><br>
        Use command: <code>ls -1 /folder</code> to get full list
        <br><br>
        {!! Form::textarea('filenames', null, ['size' => '50x10']) !!}
        <br><br>

        <font color="purple">
            {!! Form::label('Marker') !!}
        </font>
        <br><br>
        {!! Form::textarea('marker', null, ['size' => '70x5']) !!}
        <br><br>

        {!! Form::submit('Process >>', ['class' => 'btn btn-large btn-warning openbutton']) !!}
        {!! Form::close() !!}
    </p>

@stop
@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/alertSlideUp.js') }}"></script>
@stop